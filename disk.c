#include "disk.h"
#include <stdio.h>
#include <stdlib.h>

FILE *disk;
struct super_block *SB;
struct list *FreeList;

int 	BLOCK_SIZE = 512,
		NUM_BLOCKS = 256,
		POINTERS_PER_BLOCK = 512/4,
		INODES_PER_BLOCK = 512/I_NODE_SIZE,
		read_count = 0,
		write_count = 0;


int INIT(){

	SB = (struct super_block*) malloc(sizeof(struct super_block));
	
	FreeList = (struct list*) malloc(sizeof(struct list));
	inicializa_lista(FreeList);

	disk = fopen("disk", "w+b");

	
	char *empty_block = (char*) calloc(BLOCK_SIZE, sizeof(char));

	for (int i = 0; i < NUM_BLOCKS; i++){
		fwrite(empty_block, 1, BLOCK_SIZE, disk);
	}

	free(empty_block);

	read_count = write_count = 0;

/*		
		SB->size = get_block_num();
		SB->iSize = 1;
		SB->freeList = 2;

		fd_write_super_block(0, SB);

		

		

		for (int i = SB->iSize; i < SB->size; i++){
			push_back(FreeList, i);
		}
	}
*/
	return 1;
}


int seek(int block_num){
	if (block_num < 0 || block_num >= NUM_BLOCKS)
		return 0;

	return fseek(disk, (long)(block_num * BLOCK_SIZE), SEEK_SET) == 0;
}

/* Funcoes para mudar e obter o tamanho e numero de blocos simulados */
void set_block_size(int block_size){ //default 512
	BLOCK_SIZE = block_size;
	POINTERS_PER_BLOCK = BLOCK_SIZE/4;
	INODES_PER_BLOCK = BLOCK_SIZE/I_NODE_SIZE;
}

int get_block_size(){
	return BLOCK_SIZE;
}

void set_block_num(int block_num){ //default 256
	NUM_BLOCKS = block_num;
}

int get_block_num(){
	return NUM_BLOCKS;
}

int get_pointers_per_block(){
	return get_block_size()/4;
}

int get_inodes_per_block(){
	return get_block_size()/I_NODE_SIZE;
}

void fd_read_raw(int block_num, char * buffer){
	//printf("BN: %d\n",block_num);
	if (!seek(block_num) || (fread(buffer, 1, BLOCK_SIZE, disk) != (size_t)BLOCK_SIZE)){
		return;
	}

	read_count++;
}

void fd_write_raw(int block_num, char * buffer){
	if (!seek(block_num)){
		return;
	}

	//printf("BN: %d\n",block_num);

	fwrite(buffer, 1, BLOCK_SIZE, disk);

	if (ferror(disk)){
		printf("Error trying to write block %d\n", block_num);
		exit(1);
	}

	write_count++;
}

void fd_read_super_block(int block_num, struct super_block * buffer){
	if (!seek(block_num)){
		return;
	}

	fread(buffer, sizeof(struct super_block), 1, disk);

	read_count++;
}

void fd_write_super_block(int block_num, struct super_block * buffer){
	if (!seek(block_num)){
		return;
	}

	fwrite(buffer, sizeof(struct super_block), 1, disk);

	write_count++;
}

void fd_read_i_node_block(int block_num, struct i_node_block * buffer){
	if (!seek(block_num)){
		return;
	}

	fread(buffer->node, sizeof(struct i_node), INODES_PER_BLOCK, disk);	

	read_count++;
}

void fd_write_i_node_block(int block_num, struct i_node_block * buffer){
	if (!seek(block_num)){
		return;
	}

	fwrite(buffer->node, sizeof(struct i_node), INODES_PER_BLOCK, disk);

	write_count++;
}

void fd_read_indirect_block(int block_num, struct indirect_block * buffer){
	if (!seek(block_num)){
		return;
	}

	fread(buffer->pointer, sizeof(int), POINTERS_PER_BLOCK, disk);

	read_count++;
}

void fd_write_indirect_block(int block_num, struct indirect_block * buffer){
	if (!seek(block_num)){
		return;
	}

	fwrite(buffer->pointer, sizeof(int), POINTERS_PER_BLOCK, disk);

	write_count++;
}


int fd_stop(){	
	printf("\nRead Count: %d  ---  Write Count: %d\n", read_count, write_count);

	return read_count + write_count ;
}



void inicializa_lista(struct list *l){
	l->head = NULL;
	l->tail = NULL;
}

void push_back(struct list *l, int i){
	struct node *n = (struct node*) malloc(sizeof(struct node));
	n->i = i;
	n->next = NULL;

	if (l->tail == NULL){
		l->head = n;
		l->tail = n;
	}
	else{
		l->tail->next = n;
		l->tail = n;
	}
}

int pop_front(struct list *l){
	if (l->head == NULL)
		return 0;

	struct node *n = l->head;
	int i = n->i;

	l->head = n->next;

	//se também era o ultimo elemento
	if (l->head == NULL)
		l->tail = NULL;

	free(n);
	
	return i;
}

int get_front(struct list *l){

	if (l->head == NULL)
		return 0;
	
	return l->head->i;
}

/*
void print_inode(struct i_node *inode){
	printf("Flags: %d\n", inode->flags);
	printf("Owner: %d\n", inode->owner);
	printf("Size:  %d\n", inode->file_size);

	for (int i = 0; i < 13; i++){
		printf ("%d ", inode->pointer[i]);
	}
	printf("\n");

}

void print_indirect(struct indirect_block *indirect){
	for (int i = 0; i < POINTERS_PER_BLOCK; i++){
		printf ("%d ", indirect->pointer[i]);
	}
	printf("\n");
}

*/
