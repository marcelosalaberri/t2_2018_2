#ifndef _FAKEDISK_H_
#define _FAKEDISK_H_

#define I_NODE_SIZE 64


#include <stdio.h>


int INIT();

struct i_node{
	int flags;
	int owner;
	int file_size; 
	int pointer[13]; // (tamanho 13) tamanho do vetor definido aqui pois ele não varia
};


struct i_node_block{
	struct i_node *node; // tamanho = INODES_PER_BLOCK
};

struct indirect_block{
	int *pointer; // BLOCK_SIZE/4
};

struct super_block{
	int size;
	int iSize;
	int freeList;
};


int seek(int block_num);

/* Funcoes para mudar e obter o tamanho e numero de blocos simulados */
void set_block_size(int block_size); //default 512
int get_block_size();

void set_block_num(int block_num); //default 256
int get_block_num();



/* Como definido no livro */
void fd_read_raw(int block_num, char * buffer);

void fd_write_raw(int block_num, char * buffer);

void fd_read_super_block(int block_num, struct super_block * buffer);

void fd_write_super_block(int block_num, struct super_block * buffer);

void fd_read_i_node_block(int block_num, struct i_node_block * buffer);

void fd_write_i_node_block(int block_num, struct i_node_block * buffer);

void fd_read_indirect_block(int block_num, struct indirect_block * buffer);

void fd_write_indirect_block(int block_num, struct indirect_block * buffer);

int fd_stop();

///////////////////////////////////////////////////////////////////////////////////////

struct node{
	int i;
	struct node *next; 
};

struct list{
	struct node *head;
	struct node *tail;
};


void inicializa_lista(struct list *l);

void push_back(struct list *l, int i);

int pop_front(struct list *l);

int get_front(struct list *l);

void print_inode(struct i_node *inode);


//extern para evitar multiplas definições

extern FILE *disk;
extern struct super_block *SB;
extern struct list *FreeList;

extern int 	BLOCK_SIZE,
			NUM_BLOCKS,
			POINTERS_PER_BLOCK,
			INODES_PER_BLOCK,
			read_count,
			write_count;


#endif /* _FAKEDISK_H_ */
