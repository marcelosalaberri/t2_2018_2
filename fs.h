#ifndef _FAKEFS_H_
#define _FAKEFS_H_

#define FFS_SEEK_SET 0
#define FFS_SEEK_CUR 1
#define FFS_SEEK_END 2

#include "disk.h"

struct table{
	struct i_node inode;
    int i_number;			//Usando 0 para indicar que não existe arquivo no 'fd'
    int seek_ptr;
};

extern struct table FDS[21];

int ffs_format_disk(int size, int i_size);

int ffs_shutdown();

int ffs_create();

int ffs_open(int i_number);

int ffs_i_number(int fd);

int ffs_read(int fd, char * buffer, int size);

int ffs_write(int fd, char * buffer, int size);

int ffs_seek(int fd, int offset, int whence);

int ffs_close(int fd);

int ffs_delete(int i_number);


#endif /* _FAKEFS_H_ */