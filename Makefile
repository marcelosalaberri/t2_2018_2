CC=gcc

CFLAGS= -O0 -g -std=c11 -pthread
LDFLAGS=-lm

.PHONY: all

all: grade

disk.o: disk.c 
	$(CC) $(CFLAGS) -c disk.c

fs.o: fs.c 
	$(CC) $(CFLAGS) -c fs.c

test: disk.o fs.o test.c
	$(CC) $(CFLAGS) -o test disk.o fs.o test.c $(LDFLAGS)

grade: test
	./test

taluno: disk.o fs.o aluno.c
	$(CC) $(CFLAGS) -o aluno disk.o fs.o aluno.c $(LDFLAGS)

aluno: taluno
	./aluno


clean:
	rm -rf *.o teste.out
