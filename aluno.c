#include "disk.h"
#include "fs.h"
#include "simplegrade.h"
#include <stdio.h>

#define CLEANUP(a, x, size)\
	for(int k=0; k<size; k++) a[k] = x;

//Este arquivo será utilizado para testar casos que não foram cobertos no teste do professor

int main(){

	INIT();

	//Utilizar um número de blocos maior que o default (256) fez perceber que é preciso setar o número de blocos dentro da função de formatação.
	set_block_size(256);
	ffs_format_disk(512,8); // 512 blocos de 256 B = 128 KiB
	// 1 bloco para superbloco
	// 8 bloco de inodes
	// sobram 503

	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	printf("Criar arquivos enquanto disk possuir i_nodes livres\n");
	//Para o disco acima devem ser criados BLOCK_SIZE/I_NODE_SIZE * 8 = 256/64 * 8 = 32;
	int count;
	while (ffs_create()!=-1){
		count++;
	}
	if (count == 32)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", count);
	//Este teste me fez perceber que as minhas variáveis globais estavam com multipla definição, pois o valor de BLOCK_SIZE não havia mudado com o set_block_size()

	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

	//Abri arquivos enquanto houver espaço da tabela de arquivos (FDS)
	printf("\nAbrir arquivos enquanto a tabela de arquivos possuir entradas livres\n");
	count = 0;
	while(ffs_open(count) != -1){
		count++;
	}
	if (count == 21)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", count);


	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

	char buffer[2049], res[2049];
	buffer[2048] = res[2048] = '\0';
	char *p;
	int i, j, ok;
	
	CLEANUP(res, ' ', 2048);
	int fd = 0;

	printf("\nTestando a escrita e leitura utilizando tamanho menores que o BLOCK_SIZE\n");

	//Escreve o buffer no arquivo com sequencias de 128 Bytes de A,B,C...
	char c = 'A';
	for (i = 0; i < 2048; i+=128){
		p = buffer+i;
		CLEANUP(p, c, 128);
		ffs_write(fd, buffer+i, 128);
		c++;
	}

	//Volta pro inicio e tenta ler
	ffs_seek(fd, 0, 0);
	for (i = 0; i < 2048; i+=128){
		ffs_read(fd, res+i, 128);
	}

	ok = 1;
	for(i=0; i<2048; i++)
		if (buffer[i]!=res[i])
			ok = 0;

	if (ok)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", i);

	//-------------------------------------//

	printf("\nTentando ler na ordem inversa\n");
	CLEANUP(res, ' ', 2048);
	for (i = 1920; i >= 0; i-=128){
		ffs_seek(fd, i, 0);
		ffs_read(fd, res+i, 128);
	}

	ok = 1;
	for(i=0; i<2048; i++)
		if (buffer[i]!=res[i])
			ok = 0;

	if (ok)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", i);

	//-------------------------------------//
	printf("\nLendo blocos intercaladamente e testanto FFS_SEEK_CUR\n");
	CLEANUP(res, ' ', 2048);

	ffs_seek(fd, 0, 0);
	for (i = 0; i < 2048; i+=512){			//Lendo blocos pares
		ffs_read(fd, res+i, 256);
		ffs_seek(fd, 256, FFS_SEEK_CUR); 
	}
	ffs_seek(fd, 0, 0);
	for (i = 0; i < 2048; i+=512){			//Lendo blocos impares
		ffs_seek(fd, 256, FFS_SEEK_CUR);
		ffs_read(fd, res+256+i, 256);
		
	}

	ok = 1;
	for(i=0; i<2048; i++)
		if (buffer[i]!=res[i])
			ok = 0;
		
	if (ok)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", i);

	//-------------------------------------//

	printf("\nTestando sobreescrita e leitura utilizando buffers de tamanho 100\npara gerar irregularidades em como os dados serao escritos nos blocos de dados\n");
	
	//Preenche o buffer com dados diferentes e sobreescreve no arquivo.
	c = 'A' + 19;
	ffs_seek(fd, 0, 0);
	for (i = 0; i < 2000; i+=100){
		p = buffer+i;
		CLEANUP(p, c, 100);
		ffs_write(fd, buffer+i, 100);
		c--;
	}

	CLEANUP(res, ' ', 2000);
	//Volta pro inicio e tenta ler
	ffs_seek(fd, 0, 0);
	for (int i = 0; i < 2000; i+=100){
		ffs_read(fd, res+i, 100);
	}

	ok = 1;
	for(i=0; i<2048; i++)
		if (buffer[i]!=res[i])
			ok = 0;
		
	if (ok)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", i);

	//-------------------------------------//
	printf("\nTestando se o restante do arquivo (48 bytes) permaneceu como estava pois nao foi realizado sobreescrita neles\n");
	ffs_seek(fd, 2000, 0);
	ffs_read(fd, res+2000, 48);

	ok = 1;
	for(i=2000; i<2048; i++)
		if (buffer[i]!=res[i])
			ok = 0;
		
	if (ok)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", i);
	//-------------------------------------//

	printf("\nTestando leitura que 'ultrapassa' o tamanho do arquivo -- deve ler somente 48 bytes neste caso\n");
	ffs_seek(fd, 2000, 0);

	count = ffs_read(fd, res, 256);
	if (count == 48)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", count);

	//-------------------------------------//
	printf("\nTestando leitura no fim do arquivo -- nao deve conseguir ler\n");
	ffs_seek(fd, 2048, 0);

	count = ffs_read(fd, res, 256);
	if (count == 0)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", count);

	//-------------------------------------//

	printf("\nTestando escrever um arquivo com 'buracos' avançando o seek, e verificando atraves de leituras\n");
	//Este teste me fez perceber que ao avançar o seek de um arquivo é preciso verificar se o mesmo não passou do final do arquivo e ajustar o file_size de acordo
	fd = 1;
	c = 'A';
	CLEANUP(buffer, ' ', 2048);
	for (i = 0; i < 1024; i+=256){
		p = buffer + i;
		CLEANUP(p, c, 256);

		ffs_write(fd, buffer+i, 256);		//escreve em um bloco
		ffs_seek(fd, 256, FFS_SEEK_CUR);	//pula 1 bloco ('buraco')

		c++;
	}

	CLEANUP(res, ' ', 2048);
	ffs_seek(fd, 0, 0);
	for (i = 0; i < 2048; i+=256){
		ffs_read(fd, res+i, 256);
	}
	
	ok = 1;
	for (i=0, j=0; i < 1024 && ok == 1; i+=256, j+=256){  
		for (int k = 0; k < 256; k++){ //compara os buffers
			if (buffer[i+k]!=res[j+k])
				ok = 0;
		}
		j += 256;
		for (int k = 0; k < 256; k++){ //verifica se os 'buracos' lidos estão preenchidos com 0s
			if (res[j+k] != 0)
				ok = 0;
		}
	}

	if (ok)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", i);
	//-------------------------------------//

	printf("Testando escrita de um arquivo grande que gere a necessidade de apontamento duplo\n");
	fd = 2;
	int n_b = 10 + BLOCK_SIZE/4;				//quantidade de blocos que preenche o apontamento simples
	n_b += 2; 									//10 blocos no apontamento duplo

	char *big_buffer  = malloc(n_b*256);

	c = 'A';
	for (i = 0 ; i < n_b*BLOCK_SIZE; i+=32){
		p = big_buffer+i;
		CLEANUP(p, c, 32);
		c++;
		if (c > 'Z') c = 'A';
	}

	for (i = 0; i < n_b; i++){
		ffs_write(fd, big_buffer + i*256, 256);
	}

	if (FDS[fd].inode.file_size == n_b * 256)
		printf("---PASSED---\n");
	else
		printf("Fail %d\n", FDS[fd].inode.file_size);
	//-------------------------------------//

	free(big_buffer);

	return 0;
}