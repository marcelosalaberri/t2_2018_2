# Trabalho 2 - Sistemas Operacionais - 2018/2
Prof. Maurício Lima Pilla - http://lups.inf.ufpel.edu.br/~pilla

## Dados do(a)s aluno(a)s

Declaro que o presente trabalho contém código desenvolvido exclusivamente por mim e que não foi compartilhado de nenhuma forma com terceiros a não ser o professor da disciplina. Compreendo que qualquer violação destes princípios será punida rigorosamente de acordo com o Regimento da UFPEL.

(Preencha com seus dados)

- Nome completo: Marcelo Bonoto Salaberri        
- Username do Bitbucket: marcelosalaberri
- Email @inf: mbsalaberri@inf.ufpel.edu.br

- Nome completo: Marcelo Bonoto Salaberri
- Username do Bitbucket: marcelosalaberri
- Email @inf: mbsalaberri@inf.ufpel.edu.br

## Descrição

O trabalho 2 consistirá na implementação do projeto de programação __criando um sistema de arquivos__, página 393 do livro __Sistemas Operacionais com Java__, 8a. edição, de Silberschatz, Galvin e Gagne. 

O projeto original no livro é proposto para implementação em Java. Neste trabalho, os alunos poderão usar as linguagens de programação _C_ ou _C++_. 

Os arquivos _disk.h_ e _fs.h_ apresentam as assinaturas de funções e estruturas que devem ser implementadas. Sugere-se fortemente que _disk.c_ seja implementado primeiro.

Os testes do Professor serão fornecidos até 30/11/2018.

O trabalho poderá ser feito em dupla, mas ambos os alunos devem ter feito ou não o trabalho 1. **Duplas mistas terão a nota dividida por dois**.

## Produtos

Os seguintes produtos devem ser entregues:

* Implementação (*disk.c e fs.c*) -- 8 pontos 
* Casos de teses do(a) aluno(a) (*aluno.c*) -- 2 pontos (proporcional à implementação)
* Makefile e configuração do pipeline -- 0 pontos (mas trabalho não será corrigido se não estiver executando)

O pipeline do Bitbucket deve estar executando ambos os testes (primeiro o teste do aluno, depois o do professor) em duas regras:

        make aluno
        make grade

**Grupos com _commits_ predominantemente de um componente apenas terão descontos na nota.**


## Cronograma

* _disk.c_ -- 9/11/2018 (_milestone_ sugerido)
* _fs.c_  -- 26/11/2018 (_milestone_ sugerido)
* _aluno.c_ -- 30/11/2018 (_milestone_ sugerido)
* Submissão final --  04/12/2018 (_hard deadline_)
