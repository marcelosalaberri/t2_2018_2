#include "fs.h"
#include <stdlib.h>
#include <string.h>

struct table FDS[21];

int ffs_format_disk(int size, int i_size){
	if (size == 0 || i_size == 0 || i_size >= size)
		return 0;

	fclose(disk);
	
	disk = fopen("disk", "w+b");

	set_block_num (size); //precisa setar o número de blocos, pois é utilziado em outras funções

	//Altera o bloco inicial da lista de livres e escreve o super block no disco
	SB->size = size;
	SB->iSize = i_size;
	SB->freeList = SB->iSize + 1; 	//primeiro data block
	fd_write_super_block(0, SB);

	if (!seek(0))
		printf("Erro\n");

	struct i_node_block ib;
	ib.node = calloc(INODES_PER_BLOCK, sizeof(struct i_node));

	for (int i = 0; i < INODES_PER_BLOCK; i++){
		ib.node[i].flags = 0;
		ib.node[i].owner = 0;
		ib.node[i].file_size = 0;
	}

	for (int i = 1; i <= SB->iSize; i++){
		fd_write_i_node_block(i, &ib);
	}
	free(ib.node);

	char *empty_block = (char*) calloc(BLOCK_SIZE, sizeof(char));
	for (int i = SB->iSize + 1; i < SB->size; i++){
		fd_write_raw(i, empty_block);
	}
	free(empty_block);


	//Gerenciamento de Free Space

	//esvazia a lista
	while(pop_front(FreeList)){}

	
	//coloca todos os data_blocks na lista de livres
	for (int i = SB->iSize + 1; i < SB->size; i++){
		push_back(FreeList, i);
	}

	for (int i = 0; i <= 20; i++){
		FDS[i].i_number = -1;
	}

	read_count = write_count = 0;

	return 1;
}

int ffs_shutdown(){
	//fecha arquivos
	for (int fd =0; fd <= 20; fd++)
		if (FDS[fd].i_number != -1)
			ffs_close(fd);

	//para o disco
	fd_stop();
}


int ffs_create(){
	int i_number = -1;
	struct i_node in;

	struct i_node_block ib;
	ib.node = (struct i_node*) malloc(sizeof(struct i_node) * INODES_PER_BLOCK);

	//procura um i_node livre
	int i, j;
	for (i = 1; (i <= SB->iSize) && (i_number == -1); i++){
		fd_read_i_node_block(i, &ib);
		for (j = 0; (j < INODES_PER_BLOCK) && (i_number == -1); j++){
			if (ib.node[j].flags == 0){ //se está sem uso
				i_number = (i-1) * INODES_PER_BLOCK + j;
				
				ib.node[j].flags = 1;
				ib.node[j].owner = 0;
				ib.node[j].file_size = 0;

				for (int k = 0; k < 13; k++){			//faz seus ponteiros apotarem para 0 (NULO), pois se já foi usado pode conter lixo
					ib.node[j].pointer[k] = 0;
				}

				fd_write_i_node_block(i, &ib);
			}
		}
	}


	if (i_number == -1){
		return -1;
	}

	free(ib.node);

	return i_number;
}

int ffs_open(int i_number){
	int fd;
	for (fd = 0; fd <= 20; fd++){
		if (FDS[fd].i_number == -1)
			break;
	}

	if (fd == 21){
		//printf("Sem espaço na tabela de arquivos\n");
		return -1;
	}

	int index_ib = i_number/INODES_PER_BLOCK + 1; //index do i_node_block
	int index_in = i_number%INODES_PER_BLOCK; //index do i_node dentro do seu bloco

	struct i_node_block ib;
	ib.node = (struct i_node*) malloc(sizeof(struct i_node) * INODES_PER_BLOCK);

	fd_read_i_node_block(index_ib, &ib);

	/*
	if (ib.node[index_in].flags == 0){
		printf("Este i_number (%d) não corresponde a um arquivo\n", i_number);
		free(ib.node);
		return -1;
	}
	*/

	if (ib.node[index_in].flags == 0){ //Cria um arquivo se este i-number não possui um.
		ib.node[index_in].flags = 1;
		ib.node[index_in].file_size = 0;

		for (int j = 0; j < 13; j++){
			ib.node[index_in].pointer[j] = 0;
		}

		fd_write_i_node_block(index_ib, &ib);
	}

	FDS[fd].i_number = i_number;
	
	FDS[fd].inode.flags = ib.node[index_in].flags;
	FDS[fd].inode.owner = ib.node[index_in].owner;
	FDS[fd].inode.file_size = ib.node[index_in].file_size;
	for (int j = 0; j < 13; j++)
		FDS[fd].inode.pointer[j] = ib.node[index_in].pointer[j];

	FDS[fd].seek_ptr = 0;

	free(ib.node);

	return fd;
}

int ffs_i_number(int fd){
	return FDS[fd].i_number;
}

int ffs_read(int fd, char *buffer, int size){
	if (FDS[fd].seek_ptr >= FDS[fd].inode.file_size)
		return 0;

	char *aux_buffer = (char*) malloc(BLOCK_SIZE);
	struct indirect_block indirect;

	int *pointers3 = (int*) malloc(BLOCK_SIZE);
	int *pointers2 = (int*) malloc(BLOCK_SIZE);
	int *pointers =  (int*) malloc(BLOCK_SIZE);

	int block_index2, block_index, data_offset, block_num, cp_size;

	int bytes_read = 0;

	//Ajusta o parametro size para ir no máximo até o fim do arquivo
	if ((FDS[fd].seek_ptr + size) >= FDS[fd].inode.file_size)
		size = FDS[fd].inode.file_size - FDS[fd].seek_ptr;


	//Enquanto não tiver preenchido o buffer
	while (bytes_read < size){
		block_index = FDS[fd].seek_ptr/BLOCK_SIZE; 			//Qual o bloco de dados que tem que ler
		data_offset = FDS[fd].seek_ptr%BLOCK_SIZE;			//Onde o seek se encontra neste bloco

		//Número de bytes que serão transferidos deste bloco para o buffer
		cp_size = BLOCK_SIZE - data_offset;

		if ((bytes_read + cp_size) > size)						//Ajusta se 'cp_size' ultrapassa o tamanho do buffer
			cp_size = size - bytes_read;

		//////// Apontamento Direto ////////
		if (block_index < 10){
			block_num = FDS[fd].inode.pointer[block_index];
						
			if (block_num != 0){ 	//bloco válido			
				fd_read_raw(block_num, aux_buffer);								//Le o bloco de dados para um buffer auxiliar
		
				memcpy(buffer+bytes_read, aux_buffer+data_offset, cp_size);		//Copia os dados para o buffer a partir do offset até cp_size
			}
			else{ 					//buraco no arquivo
				memset(buffer+bytes_read, 0, cp_size);							//Preenche com zeros por causa do buraco
			}

			bytes_read += cp_size;								//Incrementa o contador de bytes lidos
			FDS[fd].seek_ptr += cp_size;						//Atualiza o ponteiro seek
		}
		//////// Apontamento Indireto ////////
		else if (block_index < (10+POINTERS_PER_BLOCK)) {
			block_num = FDS[fd].inode.pointer[10];
			block_index -= 10;

			indirect.pointer = pointers;
			

			if (block_num == 0){ //buraco no arquivo
				data_offset = FDS[fd].seek_ptr%BLOCK_SIZE;

				cp_size = (POINTERS_PER_BLOCK * BLOCK_SIZE) - (block_index * BLOCK_SIZE + data_offset);	//Restante do "tamanho" no buraco no arquivo

				if ((bytes_read + cp_size) > size)
					cp_size = size - bytes_read;

				memset(buffer+bytes_read, 0, cp_size);

				bytes_read += cp_size;
				FDS[fd].seek_ptr += cp_size;
			}
			else{
				fd_read_indirect_block(block_num, &indirect);					//Le o bloco com os ponteiros

				//printf("1 - Block Num: %d - %d\n", block_num, block_index);

				while (bytes_read < size && block_index < POINTERS_PER_BLOCK){	//Enquanto ainda tiver blocos para serem lidos desta lista de ponteiros
					data_offset = FDS[fd].seek_ptr%BLOCK_SIZE;		
					cp_size = BLOCK_SIZE - data_offset;

					if ((bytes_read + cp_size) > size)
						cp_size = size - bytes_read;

					block_num = indirect.pointer[block_index];
					//printf("2 - Block Num: %d - %d\n", block_num, block_index);
					if (block_num == 0){ 	//buraco no arquivo
						memset(buffer+bytes_read, 0, cp_size);	
					}
					else{					//bloco válido
						fd_read_raw(block_num, aux_buffer);
						memcpy(buffer+bytes_read, aux_buffer+data_offset, cp_size);
					}

					bytes_read += cp_size;
					FDS[fd].seek_ptr += cp_size;

					block_index++;
				}
			}
		}
		//////// Apontamento Indireto Duplo////////
		else if (block_index < (10 + POINTERS_PER_BLOCK + (POINTERS_PER_BLOCK*POINTERS_PER_BLOCK))){
			block_num = FDS[fd].inode.pointer[11];
			block_index -= (10 + POINTERS_PER_BLOCK);

			if (block_num == 0){ //buraco no arquivo
				cp_size = (POINTERS_PER_BLOCK * POINTERS_PER_BLOCK * BLOCK_SIZE) - (block_index * BLOCK_SIZE + data_offset);	//Restante do "tamanho" no buraco no arquivo

				if ((bytes_read + cp_size) > size)
					cp_size = size - bytes_read;

				memset(buffer+bytes_read, 0, cp_size);

				bytes_read += cp_size;
				FDS[fd].seek_ptr += cp_size;
			}
			else{
				indirect.pointer = pointers2;
				fd_read_indirect_block(block_num, &indirect);						//Le o bloco com os ponteiros

				block_index2 = block_index/POINTERS_PER_BLOCK;

				block_index  = block_index%POINTERS_PER_BLOCK;


				while (bytes_read < size && block_index2 < POINTERS_PER_BLOCK){

					if (pointers2[block_index2] == 0){ //buraco
						data_offset = FDS[fd].seek_ptr%BLOCK_SIZE;

						cp_size = (POINTERS_PER_BLOCK * BLOCK_SIZE) - (block_index * BLOCK_SIZE + data_offset);	//Restante do "tamanho" no buraco no arquivo

						if ((bytes_read + cp_size) > size)
							cp_size = size - bytes_read;

						memset(buffer+bytes_read, 0, cp_size);

						bytes_read += cp_size;
						FDS[fd].seek_ptr += cp_size;
					}
					else{
						indirect.pointer = pointers;
						fd_read_indirect_block(pointers2[block_index2], &indirect);

						while (bytes_read < size && block_index < POINTERS_PER_BLOCK){	//Enquanto ainda tiver blocos para serem lidos desta lista de ponteiros

							data_offset = FDS[fd].seek_ptr%BLOCK_SIZE;
							cp_size = BLOCK_SIZE - data_offset;

							if ((bytes_read + cp_size) > size)
								cp_size = size - bytes_read;

							block_num = pointers[block_index];
							if (block_num == 0){ 	//buraco no arquivo
								memset(buffer+bytes_read, 0, cp_size);	
							}
							else{					//bloco válido
								fd_read_raw(block_num, aux_buffer);
								memcpy(buffer+bytes_read, aux_buffer+data_offset, cp_size);
							}

							bytes_read += cp_size;
							FDS[fd].seek_ptr += cp_size;

							block_index++;
						}

						block_index = 0;
					}
					block_index2++;
				}
			}
		}
	}

	free(aux_buffer);
	free(pointers);
	free(pointers2);
	free(pointers3);

	return bytes_read;
}

int ffs_write(int fd, char * buffer, int size){
	char *aux_buffer = (char*) malloc(BLOCK_SIZE+1);
	aux_buffer[BLOCK_SIZE] = '\0';
	int *pointers  =  (int*) calloc(POINTERS_PER_BLOCK, 4);
	int *pointers2 =  (int*) calloc(POINTERS_PER_BLOCK, 4);

	struct indirect_block indirect;
	int wr_size, block_num, block_index, block_index2, block_offset;
	
	//Enquanto ainda faltar dados para escrever
	while (size){
		block_index = FDS[fd].seek_ptr/BLOCK_SIZE; 			//Qual o bloco de dados que tem que ler
		block_offset = FDS[fd].seek_ptr%BLOCK_SIZE;			//Onde o seek se encontra neste bloco

		if (block_index < 10){
			block_num = FDS[fd].inode.pointer[block_index];
						
			if (block_num == 0){ 	//bloco vazio - alocar bloco de dados
				block_num = pop_front(FreeList);
				if (block_num == 0){
					return -1;
				}
				FDS[fd].inode.pointer[block_index] = block_num;
			}

			wr_size = BLOCK_SIZE - block_offset;

			if (wr_size > size)										//Ajusta se 'wr_size' ultrapassa o tamanho do buffer
				wr_size = size;
			
			if (wr_size != BLOCK_SIZE){				
				fd_read_raw(block_num, aux_buffer);					//O bloco já contém dados, coloca eles no buffer auxiliar

				memcpy(aux_buffer+block_offset, buffer, wr_size);	//Preenche o buffer auxiliar

				fd_write_raw(block_num, aux_buffer);				//Escreve no bloco
			}
			else{
				fd_write_raw(block_num, buffer);					//Escreve no bloco passando o buffer direto
			}

			buffer += wr_size;
			size -= wr_size;
			FDS[fd].seek_ptr += wr_size;
		}
		else if (block_index < (10+POINTERS_PER_BLOCK)) {
			block_num = FDS[fd].inode.pointer[10];
			block_index -= 10;

			if (block_num == 0){ //vazio - alocar bloco de ponteiros
				block_num = pop_front(FreeList);
				if (block_num == 0){
					return -1;
				}
				
				FDS[fd].inode.pointer[10] = block_num;

				indirect.pointer = pointers;

				fd_write_indirect_block(block_num, &indirect);		//Precisa estar com os ponteiros zerados quando escrito
			}

			fd_read_indirect_block(block_num, &indirect);			//Le o bloco com os ponteiros

			while (size && block_index < POINTERS_PER_BLOCK){		//Enquanto ainda tiver blocos para serem lidos desta lista de ponteiros
				block_num = indirect.pointer[block_index];
				block_offset = FDS[fd].seek_ptr%BLOCK_SIZE;


				if (block_num == 0){ 								//aponta para nulo
					block_num = pop_front(FreeList);
					if (block_num == 0){
						return -1;
					}

					indirect.pointer[block_index] = block_num;
				}
				

				wr_size = BLOCK_SIZE - block_offset;

				if (wr_size > size)										//Ajusta se 'wr_size' ultrapassa o tamanho do buffer
					wr_size = size;
				
				
				if (wr_size != BLOCK_SIZE){				
					fd_read_raw(block_num, aux_buffer);					//O bloco já contém dados, coloca eles no buffer auxiliar

					memcpy(aux_buffer+block_offset, buffer, wr_size);	//Preenche o buffer auxiliar

					fd_write_raw(block_num, aux_buffer);				//Escreve no bloco
				}
				else{
					fd_write_raw(block_num, buffer);					//Escreve no bloco passando o buffer direto
				}

				buffer += wr_size;
				size -= wr_size;
				FDS[fd].seek_ptr += wr_size;

				block_index++;
			}

			fd_write_indirect_block(FDS[fd].inode.pointer[10], &indirect);				//Ponteiros podem ter sido adicionados
		}
		//////// Apontamento Indireto Duplo////////
		else if (block_index < (10 + POINTERS_PER_BLOCK + (POINTERS_PER_BLOCK*POINTERS_PER_BLOCK))){
			//printf("DUPLO %d - %d\n", block_index, size);
			block_num = FDS[fd].inode.pointer[11];
			block_index -= (10 + POINTERS_PER_BLOCK);

			if (block_num == 0){ //alocar
				block_num = pop_front(FreeList);
				//printf("1 - Block Num %d alocado\n", block_num);
				if (block_num == 0){
					return -1;
				}
				
				FDS[fd].inode.pointer[11] = block_num;

				//memset(pointers, 0, BLOCK_SIZE);

				indirect.pointer = pointers2;

				fd_write_indirect_block(block_num, &indirect);
			}

			int indirect_index2 = block_num;

			fd_read_indirect_block(block_num, &indirect);

			pointers2 = indirect.pointer;

				
			block_index2 = block_index/POINTERS_PER_BLOCK;
			block_index  = block_index%POINTERS_PER_BLOCK;

			while (size && block_index2 < POINTERS_PER_BLOCK){

				block_num = pointers2[block_index2];

				//printf("BLock Num %d - ", block_num);

				if (block_num == 0){							//aponta para nulo
					block_num = pop_front(FreeList);
					if (block_num == 0){
						return -1;
					}
					pointers2[block_index2] = block_num;

					memset(pointers, 0, BLOCK_SIZE);
					indirect.pointer = pointers;

					fd_write_indirect_block(block_num, &indirect);
				}
				fd_read_indirect_block(block_num, &indirect);

				pointers = indirect.pointer;

				int indirect_index1 = block_num;

				while (size && block_index < POINTERS_PER_BLOCK){		//Enquanto ainda tiver blocos para serem lidos desta lista de ponteiros
					block_num = pointers[block_index];
					block_offset = FDS[fd].seek_ptr%BLOCK_SIZE;

					if (block_num == 0){ 								//aponta para nulo
						block_num = pop_front(FreeList);
						if (block_num == 0){
							return -1;
						}

						pointers[block_index] = block_num;
					}
					
					wr_size = BLOCK_SIZE - block_offset;

					if (wr_size > size)										//Ajusta se 'wr_size' ultrapassa o tamanho do buffer
						wr_size = size;
					
					
					if (wr_size != BLOCK_SIZE){
						fd_read_raw(block_num, aux_buffer);					//O bloco já contém dados, coloca eles no buffer auxiliar

						memcpy(aux_buffer+block_offset, buffer, wr_size);	//Preenche o buffer auxiliar

						fd_write_raw(block_num, aux_buffer);				//Escreve no bloco
					}
					else{
						fd_write_raw(block_num, buffer);					//Escreve no bloco passando o buffer direto
					}

					buffer += wr_size;
					size -= wr_size;
					FDS[fd].seek_ptr += wr_size;

					block_index++;

				}
				indirect.pointer = pointers;
				fd_write_indirect_block(indirect_index1, &indirect);				//Ponteiros podem ter sido adicionados

				block_index2++;
			}
			indirect.pointer = pointers2;
			fd_write_indirect_block(FDS[fd].inode.pointer[11], &indirect);				//Ponteiros podem ter sido adicionados
		}
		else{
			printf("Triplo\n");
		}
	}

	
	if (FDS[fd].seek_ptr > FDS[fd].inode.file_size)
		FDS[fd].inode.file_size = FDS[fd].seek_ptr;

	free(aux_buffer);
	free(pointers);
	free(pointers2);

	return FDS[fd].seek_ptr;
}

int ffs_seek(int fd, int offset, int whence){
	switch (whence){
		case FFS_SEEK_SET:
			FDS[fd].seek_ptr = offset;
			break;
		case FFS_SEEK_CUR:
			FDS[fd].seek_ptr += offset;
			if (FDS[fd].inode.file_size < FDS[fd].seek_ptr)
				FDS[fd].inode.file_size = FDS[fd].seek_ptr;
			break;
		case FFS_SEEK_END:
			FDS[fd].seek_ptr = FDS[fd].inode.file_size + offset;
			break;
	}
}


int ffs_close(int fd){
	int index_ib = FDS[fd].i_number/INODES_PER_BLOCK + 1;
	int index_in = FDS[fd].i_number%INODES_PER_BLOCK;

	struct i_node_block ib;
	ib.node = (struct i_node*) malloc(sizeof(struct i_node) * INODES_PER_BLOCK);
	
	fd_read_i_node_block(index_ib, &ib);	//le o bloco de i_nodes

	FDS[fd].i_number = -1;

	ib.node[index_in].file_size = FDS[fd].inode.file_size;		//atualiza no bloco o inode que está sendo fechado
	for (int i = 0; i < 13; i++)
		ib.node[index_in].pointer[i] = FDS[fd].inode.pointer[i];

	fd_write_i_node_block(index_ib, &ib);	//salva o bloco de i_nodes
	free(ib.node);

	return 1;
}

int ffs_delete(int i_number){

	struct indirect_block indirect;
	int *pointers =  (int*) calloc(POINTERS_PER_BLOCK, 4);
	int *pointers2 =  (int*) calloc(POINTERS_PER_BLOCK, 4);

	struct i_node_block ib;
	ib.node = (struct i_node*) malloc(sizeof(struct i_node) * INODES_PER_BLOCK);

	for (int i = 0; i <= 20; i++)
		if (FDS[i].i_number == i_number)
			FDS[i].i_number = -1;


	int num_ib = i_number/INODES_PER_BLOCK + 1;
	int num_in = i_number%INODES_PER_BLOCK;

	fd_read_i_node_block(num_ib, &ib);

	int num_blocks = (ib.node[num_in].file_size-1)/BLOCK_SIZE + 1;


	int block_idx = 0;
	int pointer_idx = 0;
	int pointer2_idx = 0;
	int block_num = 0;

	while (block_idx < num_blocks){
		if (block_idx < 10){
			block_num = ib.node[num_in].pointer[block_idx];
			if (block_num){
				push_back(FreeList, block_num);
			}
			ib.node[num_in].pointer[block_idx] = 0;

			block_idx++;
		}
		else if (block_idx < (10+POINTERS_PER_BLOCK)){
			block_num = ib.node[num_in].pointer[10];
			if (block_num == 0){
				block_idx += POINTERS_PER_BLOCK;
			}
			else{
				push_back(FreeList, block_num);

				indirect.pointer = pointers;
				fd_read_indirect_block(block_num, &indirect);

				pointer_idx = 0;
				while (block_idx < num_blocks && pointer_idx < POINTERS_PER_BLOCK){
					block_num = indirect.pointer[pointer_idx];

					if (block_num){
						push_back(FreeList, block_num);
					}

					pointer_idx++;
					block_idx++;
				}
			}
		}
		else if (block_idx < (10+POINTERS_PER_BLOCK + POINTERS_PER_BLOCK*POINTERS_PER_BLOCK)){
			block_num = ib.node[num_in].pointer[11];
			if (block_num == 0){
				block_idx += POINTERS_PER_BLOCK * POINTERS_PER_BLOCK;
			}
			else{
				push_back(FreeList, block_num);

				indirect.pointer = pointers2;
				fd_read_indirect_block(block_num, &indirect);

				pointer2_idx = 0;
				while (block_idx < num_blocks && pointer2_idx < POINTERS_PER_BLOCK){
					block_num = pointers2[pointer2_idx];

					if (block_num == 0){
						block_idx += POINTERS_PER_BLOCK;
						pointer2_idx = POINTERS_PER_BLOCK;
					}
					else{
						push_back(FreeList, block_num);

						indirect.pointer = pointers;
						fd_read_indirect_block(block_num, &indirect);

						pointer_idx = 0;
						while (block_idx < num_blocks && pointer_idx < POINTERS_PER_BLOCK){
							block_num =  pointers[pointer_idx];

							if (block_num == 0){
								push_back(FreeList, block_num);
							}

							pointer_idx++;
							block_idx++;
						}
					}
				}
			}
		}	
	}


	ib.node[num_in].flags = 0;
	ib.node[num_in].file_size = 0;

	fd_write_i_node_block(num_ib, &ib);

	free(ib.node);
	free(pointers);

	return -1;
}

